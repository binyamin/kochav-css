# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adhere to
[Semantic Versioning (SemVer)](https://semver.org/spec/v2.0.0.html).

<!--
	**Added** for new features.
	**Changed** for changes in existing functionality.
	**Deprecated** for soon-to-be removed features.
	**Removed** for now removed features.
	**Fixed** for any bug fixes.
	**Security** in case of vulnerabilities.
-->

## Unreleased

### Removed

- Removed global hyperlink styles. It was annoying to override. Besides, the idea of `src/global` is that you shouldn't _need_ to override it.

## [0.1.1] - 2024-01-24

### Added

- Added a rule in `src/reset.css`. When adjusting the size of an image, the image will now crop itself instead of stretching.

### Fixed

- Invalid `export` field in `package.json`

## [0.1.0] - 2023-11-02

### Added

- CSS Reset
- 2 utility classes
- 6 layout classes
- config (base & fluid)
- global css

[0.1.0]: https://gitlab.com/binyamin/kochav-css/-/tags/v0.1.0
[0.1.1]: https://gitlab.com/binyamin/kochav-css/-/tags/v0.1.1
